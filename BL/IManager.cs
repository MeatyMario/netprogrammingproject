﻿using System;
using System.Collections.Generic;
using Spaceflight.BL.Domain;

namespace Spaceflight.BL
{
    public interface IManager
    {
        Astronaut GetAstronaut(int id);
        Mission GetMission(int id);
        Agency GetAgency(int id);
        
        IEnumerable<Astronaut> GetAllAstronauts();
        IEnumerable<Mission> GetAllMissions();

        IEnumerable<Astronaut> GetAstronautsByNameAndDob(string name, string dob);
        IEnumerable<Mission> GetMissionsByType(Domain.MissionType missionType);

        IEnumerable<Astronaut> GetAllAstronautsWithAgency();
        IEnumerable<Mission> GetAllMissionsWithAstronauts();

        IEnumerable<Agency> GetAllAgencies();

        Astronaut AddAstronaut(string name, DateTime birthyear, string gender, Agency agency);
        Mission AddMission(string name, MissionType missionType);
        
        IEnumerable<Mission> GetMissionsWithAstronaut(int id);

        void AddAstronautMission(AstronautMission astronautMission);
        void RemoveAstronautMission(int astronautId, int missionId);

        IEnumerable<Mission> GetAstronautWithMissions(int id);
        IEnumerable<Mission> GetAstronautWithoutMissions(int id);
    }
}