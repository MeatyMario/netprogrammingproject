﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Spaceflight.BL.Domain;
using Spaceflight.DAL;
using Spaceflight.DAL.EF;

namespace Spaceflight.BL
{
    public class Manager : IManager
    {
        private readonly IRepository _repository;

        public Manager()
        {
            _repository = new Repository();
        }
        
        public Astronaut GetAstronaut(int id)
        {
            return _repository.ReadAstronaut(id);
        }

        public Mission GetMission(int id)
        {
            return _repository.ReadMission(id);
        }

        public Agency GetAgency(int id)
        {
            return _repository.ReadAgency(id);
        }

        public IEnumerable<Astronaut> GetAllAstronauts()
        {
            return _repository.ReadAllAstronauts();
        }

        public IEnumerable<Mission> GetAllMissions()
        {
            return _repository.ReadAllMissions();
        }

        public IEnumerable<Astronaut> GetAstronautsByNameAndDob(string name, string dob)
        {
            return _repository.ReadAstronautsByNameAndDob(name, dob);
        }

        public IEnumerable<Mission> GetMissionsByType(MissionType missionType)
        {
            return _repository.ReadMissionsByType(missionType);
        }

        public IEnumerable<Astronaut> GetAllAstronautsWithAgency()
        {
            return _repository.ReadAllAstronautsWithAgency();
        }

        public IEnumerable<Mission> GetAllMissionsWithAstronauts()
        {
            return _repository.ReadAllMissionsWithAstronauts();
        }

        public IEnumerable<Agency> GetAllAgencies()
        {
            return _repository.ReadAllAgencies();
        }

        public Astronaut AddAstronaut(string name, DateTime birthyear, string gender, Agency agency)
        {
            Astronaut astronaut = new Astronaut(name, birthyear, gender, agency);
            Validator.ValidateObject(astronaut, new ValidationContext(astronaut), true);
            _repository.CreateAstronaut(astronaut);
            return astronaut;
        }

        public Mission AddMission(string name, MissionType missionType)
        {
            Mission mission = new Mission(name, missionType);
            mission.Id = GetAllMissions().Count() + 1;
            _repository.CreateMission(mission);
            return mission;
        }

        public IEnumerable<Mission> GetMissionsWithAstronaut(int id)
        {
            return _repository.ReadMissionsWithAstronaut(id);
        }

        public void AddAstronautMission(AstronautMission astronautMission)
        {
            _repository.CreateAstronautMission(astronautMission);
        }

        public void RemoveAstronautMission(int astronautId, int missionId)
        {
            _repository.DeleteAstronautMission(astronautId, missionId);
        }

        public IEnumerable<Mission> GetAstronautWithMissions(int id)
        {
            return _repository.ReadAstronautWithMissions(id);
        }

        public IEnumerable<Mission> GetAstronautWithoutMissions(int id)
        {
            return _repository.ReadAstronautWithoutMissions(id);
        }
    }
}