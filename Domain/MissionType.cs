﻿namespace Spaceflight.BL.Domain
{
    public enum MissionType
    {
        Other, Exploration, Resupply, Scientific, Transport
    }
}