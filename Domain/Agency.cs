﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Spaceflight.BL.Domain
{
    public class Agency
    {
        public string Name { get; set; } 
        public string Country { get; set; }
        
        public double? Budget { get; set; }
        public int Id { get; set; }
        
        public ICollection<Astronaut> Astronauts { get; set; }
        
        public Agency()
        {
        }

        public Agency(string name, string country, double budget)
        {
            Name = name;
            Country = country;
            Budget = budget;
        }
    }
}