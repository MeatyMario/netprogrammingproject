﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Spaceflight.BL.Domain
{
    public class Astronaut : IValidatableObject
    {
        [Required]
        [StringLength(25, ErrorMessage = "Only 25 Characters allowed.")]
        public string Name { get; set; }
        public DateTime BirthYear { get; set; }
        public string Gender { get; set; }
        public Agency Agency { get; set; }
        
        [NotMapped]
        public ICollection<AstronautMission> Missions { get; set; }

        public Astronaut()
        {
        }

        public int Id { get; set; }

        IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            List<ValidationResult> errors = new List<ValidationResult>();
            if (BirthYear >= DateTime.Now)
            {
                errors.Add(new ValidationResult("Date of Birth can not be greater than today!", 
                    new string[] {"BirthYear","DateTime.Now"}));
            }
            return errors;
        }
        
        public Astronaut(string name, DateTime birthYear, string gender, Agency agency)
        {
            Name = name;
            Agency = agency;
            BirthYear = birthYear;
            Gender = gender;
            Missions = new List<AstronautMission>();
        }


    }
}