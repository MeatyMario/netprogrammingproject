﻿using System.ComponentModel.DataAnnotations;

namespace Spaceflight.BL.Domain
{
    public class AstronautMission
    {
        [Required]
        public Astronaut Astronaut { get; set; }
        [Required]
        public Mission Mission { get; set; }
    }
}