﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Spaceflight.BL.Domain
{
    public class Mission
    {
        [MinLength(4)]
        [MaxLength(35)]
        public string MissionName { get; set; }
        [Required]
        public MissionType MissionType { get; set; }
        [NotMapped]
        public ICollection<AstronautMission> Astronauts{ get; set; }
        
        public int Id { get; set; }

        public Mission(string missionName, MissionType missionType)
        {
            MissionName = missionName;
            MissionType = missionType;
            Astronauts = new List<AstronautMission>();
        }

        public Mission()
        {
        }
    }
}