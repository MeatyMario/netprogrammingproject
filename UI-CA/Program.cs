﻿using System;
using System.ComponentModel.DataAnnotations;
using Spaceflight.BL;
using Spaceflight.BL.Domain;
using Spaceflight.UI.CA.Extensions;

namespace Spaceflight.UI.CA
{
    class Program
    {
        private static IManager manager = new Manager();
        private static int choice;
        
        static void Main(string[] args)
        {
            do
            {
                ShowMenu();
                switch (choice)
                {
                    case 0: 
                        break;
                    case 1: 
                        ShowAllAstronauts();
                        break;
                    case 2: 
                        ShowCertainAstronauts();
                        break;
                    case 3: 
                        ShowAllMissions();
                        break;
                    case 4: 
                        ShowCertainMissions();
                        break;
                    case 5:
                        AddAstronaut();
                        break;
                    case 6:
                        AddMission();
                        break;
                    case 7:
                        AddAstronautToMission();
                        break;
                    case 8:
                        RemoveAstronautFromMission();
                        break;
                    default:
                        Console.WriteLine("Invalid input");
                        break;
                }
            } while (choice != 0);
        }

        private static void RemoveAstronautFromMission()
        {
            Console.WriteLine("Which astronaut would you like to remove?");
            foreach (var astronaut in manager.GetAllAstronauts())
            {
                Console.WriteLine(astronaut.GetAstronautString());
            }
            Console.WriteLine("Please enter an astronaut ID: ");
            Int32.TryParse(Console.ReadLine(), out int astronautId);
            foreach (var mission in manager.GetMissionsWithAstronaut(astronautId))
            {
                Console.WriteLine(mission.GetMissionStringWithId());
            }
            Console.WriteLine("Please enter a mission ID: ");
            Int32.TryParse(Console.ReadLine(), out int missionId);
            manager.RemoveAstronautMission(astronautId, missionId);
        }

        private static void AddAstronautToMission()
        {
            Console.WriteLine("Which astronaut would you like to add a Mission to?");
            foreach (var astronaut in manager.GetAllAstronauts())
            {
                Console.WriteLine(astronaut.GetAstronautString());
            }
            Console.WriteLine("Please enter an astronaut ID: ");
            Int32.TryParse(Console.ReadLine(), out int astronautId);
            foreach (var mission in manager.GetAllMissions())
            {
                Console.WriteLine(mission.GetMissionStringWithId());
            }
            Console.WriteLine("Please enter a mission ID that you'd like to assign to this Astronaut: ");
            Int32.TryParse(Console.ReadLine(), out int missionId);
            Astronaut chosenAstronaut = manager.GetAstronaut(astronautId);
            Mission chosenMission = manager.GetMission(missionId);
            AstronautMission astronautMission = new AstronautMission()
            {
                Astronaut = chosenAstronaut,
                Mission = chosenMission
            };
            manager.AddAstronautMission(astronautMission);
        }

        private static void AddAstronaut()
        {
            Console.WriteLine();
            Console.WriteLine("Add an Astronaut:");
            Console.WriteLine("=================");
            Console.WriteLine("Fill in his full name: ");
            string name = Console.ReadLine();
            
            Console.WriteLine("Enter a full date (yyyy/mm/dd): ");
            DateTime date = DateTime.Parse(Console.ReadLine());
            
            Console.WriteLine("Male/Female?");
            string gender = Console.ReadLine();

            Console.WriteLine("Choose an agency:");
            Agency custom = new Agency("Custom Agency", "Narnia", 69);
            foreach (Agency agency in manager.GetAllAgencies())
            {
                Console.WriteLine(agency.GetAgencyString());
            }
            
            Console.WriteLine("Please enter an Agency ID: ");
            Int32.TryParse(Console.ReadLine(), out int agencyId);

            Agency chosenAgency = manager.GetAgency(agencyId);
            
            try
            {
                manager.AddAstronaut(name, date, gender, chosenAgency);
            }
            catch (ValidationException ve)
            {
                Console.WriteLine("\n" + ve.Message);
                Console.WriteLine("Astronaut not added.");
            }
            
        }

        private static void AddMission()
        {
            Console.WriteLine();
            Console.WriteLine("Add a Mission:");
            Console.WriteLine("==============");
            Console.WriteLine("Enter the Mission Name:");
            string name = Console.ReadLine();
            
            Console.WriteLine("Choose the Mission Type:");
            Console.WriteLine("1)Exploration, 2)Resupply, 3)Scientific, 4)Transport, 5)Other");
            int choiceMission = Convert.ToInt32(Console.ReadLine());
            MissionType missionTypeMission;
            switch (choiceMission)
            {
                case 1:
                    missionTypeMission = MissionType.Exploration;
                    break;
                case 2:
                    missionTypeMission = MissionType.Resupply;
                    break;
                case 3:
                    missionTypeMission = MissionType.Scientific;
                    break;
                case 4:
                    missionTypeMission = MissionType.Transport;
                    break;
                case 5:
                    missionTypeMission = MissionType.Other;
                    break;
                default:
                    missionTypeMission = MissionType.Other;
                    break;
            }
            manager.AddMission(name, missionTypeMission);
        }

        private static void ShowCertainMissions()
        {
            Console.WriteLine();
            Console.WriteLine("Choose your mission type:");
            Console.WriteLine("1)Exploration, 2)Resupply, 3)Scientific, 4)Transport, 5)Other");

            int choiceMission = Convert.ToInt32(Console.ReadLine());
            MissionType missionTypeMission;
            switch (choiceMission)
            {
                case 1:
                    missionTypeMission = MissionType.Exploration;
                    break;
                case 2:
                    missionTypeMission = MissionType.Resupply;
                    break;
                case 3:
                    missionTypeMission = MissionType.Scientific;
                    break;
                case 4:
                    missionTypeMission = MissionType.Transport;
                    break;
                case 5:
                    missionTypeMission = MissionType.Other;
                    break;
                default:
                    missionTypeMission = MissionType.Other;
                    break;
            }
            foreach (Mission mission in manager.GetMissionsByType(missionTypeMission))
            {
                Console.WriteLine(mission.GetMissionString());
            }
        }

        private static void ShowAllMissions()
        {
            Console.WriteLine();
            Console.WriteLine("All Missions");
            Console.WriteLine("============");
            foreach (Mission mission in manager.GetAllMissionsWithAstronauts())
            {
                Console.WriteLine(mission.GetMissionStringWithAstronauts());
            }
        }

        private static void ShowCertainAstronauts()
        {
           Console.Write("Enter (part of) a name or leave blank: ");
           string name = Console.ReadLine();
           
           Console.Write("Enter a full date (yyyy/mm/dd) or leave blank: ");
           string dob = Console.ReadLine();

           foreach (Astronaut astronaut in manager.GetAstronautsByNameAndDob(name, dob))
           {
               Console.WriteLine(astronaut.GetAstronautStringWithoutAgency());
           }
        }

        private static void ShowAllAstronauts()
        {
            Console.WriteLine();
            Console.WriteLine("All Astronauts");
            Console.WriteLine("==============");
            foreach (Astronaut astronaut in manager.GetAllAstronautsWithAgency())
            {
                Console.WriteLine(astronaut.GetAstronautStringWithMissions());
            }
        }

        private static void ShowMenu()
        {
            Console.WriteLine();
            Console.WriteLine("What would you like to do?");
            Console.WriteLine("==========================");
            Console.WriteLine("0) Quit");
            Console.WriteLine("1) Show all Astronauts");
            Console.WriteLine("2) Show Astronauts with certain Name and/or DOB");
            Console.WriteLine("3) Show all Missions");
            Console.WriteLine("4) Show Missions with certain Type");
            Console.WriteLine("5) Add an Astronaut");
            Console.WriteLine("6) Add a Mission");
            Console.WriteLine("7) Add Astronaut to Mission");
            Console.WriteLine("8) Remove Astronaut from Mission");
            Console.Write("Choice (0-6): ");
            choice = Convert.ToInt32(Console.ReadLine());
        }
    }
}