﻿using Spaceflight.BL.Domain;

namespace Spaceflight.UI.CA.Extensions
{
    public static class MissionExtensions
    {
        public static string GetMissionString(this Mission mission)
        {
            return
                $"{mission.MissionName} " +
                $"({mission.MissionType})";
        }
        
        public static string GetMissionStringWithId(this Mission mission)
        {
            return
                $"[{mission.Id}]\t " +
                $"{mission.MissionName} ";
        }
        
        public static string GetMissionStringWithAstronauts(this Mission mission)
        {
            string astronautString =
                $"\n{mission.MissionName} " +
                $"({mission.MissionType})";
            foreach (var astronaut in mission.Astronauts)
            {
                astronautString += $"\n\t-Astronaut: {astronaut.Astronaut.Name}";
            }

            return astronautString;
        }
    }
}