﻿using Spaceflight.BL.Domain;

namespace Spaceflight.UI.CA.Extensions
{
    public static class AstronautExtensions
    {
        public static string GetAstronautString(this Astronaut astronaut)
        {
            return
                $"[{astronaut.Id}]\t " +
                $"{astronaut.Name,-25} ";
        }

        public static string GetAstronautStringWithoutAgency(this Astronaut astronaut)
        {
            return
                $"{astronaut.Name,-25} " +
                $"{astronaut.BirthYear,-12:yyyy/MM/dd}" +
                $"{astronaut.Gender,-8}";

        }
        public static string GetAstronautStringWithMissions(this Astronaut astronaut)
        {
            string astronautString =
                $"\n{astronaut.Name,-25} " +
                $"{astronaut.BirthYear, -12:yyyy/MM/dd}" +
                $"{astronaut.Gender, -8}" +
                $"{astronaut.Agency.Name}";
            foreach (var missions in astronaut.Missions)
            {
                astronautString += $"\n\t-Mission: {missions.Mission.MissionName} ({missions.Mission.MissionType}) ";
            }

            return astronautString;
        }
    }
}