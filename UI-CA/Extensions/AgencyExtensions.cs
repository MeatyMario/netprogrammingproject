﻿using System;
using Spaceflight.BL.Domain;

namespace Spaceflight.UI.CA.Extensions
{
    public static class AgencyExtensions
    {
        public static string GetAgencyString(this Agency agency)
        {
            return $"[{agency.Id}] {agency.Name}";
        }
    }
}