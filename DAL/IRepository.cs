﻿using System;
using System.Collections.Generic;
using Spaceflight.BL.Domain;

namespace Spaceflight.DAL
{
    public interface IRepository
    {
        Astronaut ReadAstronaut(int id);
        Mission ReadMission(int id);
        Agency ReadAgency(int id);

        IEnumerable<Astronaut> ReadAllAstronauts();
        IEnumerable<Mission> ReadAllMissions();

        IEnumerable<Astronaut> ReadAstronautsByNameAndDob(string name = null, string dob = null);
        IEnumerable<Mission> ReadMissionsByType(MissionType missionType);

        void CreateAstronaut(Astronaut astronaut);
        void CreateMission(Mission mission);

        IEnumerable<Astronaut> ReadAllAstronautsWithAgency();
        IEnumerable<Mission> ReadAllMissionsWithAstronauts();

        void CreateAstronautMission(AstronautMission astronautMission);
        void DeleteAstronautMission(int astronautId, int missionId);

        IEnumerable<Mission> ReadMissionsWithAstronaut(int id);

        IEnumerable<Mission> ReadAstronautWithMissions(int id);
        
        IEnumerable<Mission> ReadAstronautWithoutMissions(int id);

        IEnumerable<Agency> ReadAllAgencies();

    }
}