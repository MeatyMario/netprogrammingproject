﻿using System;
using System.Collections.Generic;
using Spaceflight.BL.Domain;

namespace Spaceflight.DAL
{
    public class InMemoryRepository : IRepository
    {
        public static ICollection<BL.Domain.Astronaut> Astronauts { get; } = new List<BL.Domain.Astronaut>();
        public static ICollection<Mission> Missions { get; } = new List<Mission>();

        static InMemoryRepository()
        {
            Seed();
        }
        
        public BL.Domain.Astronaut ReadAstronaut(int id)
        {
            foreach (BL.Domain.Astronaut astronaut in Astronauts)
            {
                if (astronaut.Id == id)
                {
                    return astronaut;
                }
            }

            return null;
        }

        public Mission ReadMission(int id)
        {
            foreach (Mission mission in Missions)
            {
                if (mission.Id == id)
                {
                    return mission;
                }
            }

            return null;
        }

        public Agency ReadAgency(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<BL.Domain.Astronaut> ReadAllAstronauts()
        {
            return Astronauts as List<BL.Domain.Astronaut>;
        }

        public IEnumerable<Mission> ReadAllMissions()
        {
            return Missions as List<Mission>;
        }

        public IEnumerable<BL.Domain.Astronaut> ReadAstronautsByNameAndDob(string name, string date)
        {
            List<BL.Domain.Astronaut> astronautsByNameAndDob = new List<BL.Domain.Astronaut>();
            bool nameIsEmpty = String.IsNullOrEmpty(name);
            bool dateIsEmpty = String.IsNullOrEmpty(date.ToString());

            foreach (BL.Domain.Astronaut astronaut in Astronauts)
            {
                if (nameIsEmpty && dateIsEmpty)
                {
                    astronautsByNameAndDob.Add(astronaut);
                }

                if (nameIsEmpty && !dateIsEmpty)
                {
                    if (astronaut.BirthYear.Equals(date))
                    {
                        astronautsByNameAndDob.Add(astronaut);
                    }
                }

                if (!nameIsEmpty && dateIsEmpty)
                {
                    if (astronaut.Name.ToLower().Contains(name.ToLower()))
                    {
                        astronautsByNameAndDob.Add(astronaut);
                    }
                }

                if (!nameIsEmpty && !dateIsEmpty)
                {
                    if (astronaut.Name.ToLower().Contains(name.ToLower()) &&
                        astronaut.BirthYear.Equals(date))
                    {
                        astronautsByNameAndDob.Add(astronaut);
                    }
                }
            }

            return astronautsByNameAndDob;
        }

        public IEnumerable<Mission> ReadMissionsByType(BL.Domain.MissionType missionType)
        {
            List<Mission> missionsByType = new List<Mission>();

            foreach (Mission mission in Missions)
            {
                if (mission.MissionType == missionType)
                {
                    missionsByType.Add(mission);
                }
            }

            return missionsByType;
        }

        public void CreateAstronaut(BL.Domain.Astronaut astronaut)
        {
            astronaut.Id = Astronauts.Count + 1;
            Astronauts.Add(astronaut);
        }

        public void CreateMission(Mission mission)
        {
            mission.Id = Missions.Count + 1;
            Missions.Add(mission);
        }

        public IEnumerable<BL.Domain.Astronaut> ReadAllAstronautsWithAgency()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Mission> ReadAllMissionsWithAstronauts()
        {
            throw new NotImplementedException();
        }

        public void CreateAstronautMission(AstronautMission astronautMission)
        {
            throw new NotImplementedException();
        }

        public void DeleteAstronautMission(int astronautId, int missionId)
        {
            
        }

        public void DeleteAstronautMission(string astronautName, string missionName)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Mission> ReadMissionsWithAstronaut(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Mission> ReadAstronautWithMissions(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Mission> ReadAstronautWithoutMissions(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Agency> ReadAllAgencies()
        {
            throw new NotImplementedException();
        }

        /*public IEnumerable<BL.Domain.Astronaut> ReadAstronautWithMissions(int id)
        {
            throw new NotImplementedException();
        }*/

        public static void Seed()
        {
            //Make Agencies
            Agency nasa = new Agency("NASA", "United States of America", 22.6);
            Agency csa = new Agency("CSA", "Canada", 0.332);
            Agency esa = new Agency("ESA", "European Union", 6.68);
            Agency roscosmos = new Agency("Roscosmos", "Russia", 2.4);
            Agency jaxa = new Agency("JAXA", "Japan", 1.7);

            //Make Astronauts
            /*Astronaut dougHurley = new Astronaut("Dough Hurley", new DateTime(1966,10,21), "Male");
            Astronaut robertBehnken = new Astronaut("Robert Behnken", new DateTime(1970,7,28), "Male");
            Astronaut chrisHadfield = new Astronaut("Chris Hadfield", new DateTime(1959, 8, 29), "Male");
            Astronaut samanthaCristoforetti = new Astronaut("Samantha Cristoforetti", new DateTime(1977,4,26), "Female");
            Astronaut antonShkaplerov = new Astronaut("Anton Shkaplerov", new DateTime(1972,2,20), "Male");
            */

            /*dougHurley.Id = 0;
            robertBehnken.Id = 1;
            chrisHadfield.Id = 2;
            samanthaCristoforetti.Id = 3;
            antonShkaplerov.Id = 4;*/
            
            
            //Make Missions
            /*Mission expedition43 = new Mission("Expedition 43", Type.Transport);
            expedition43.Astronauts.Add(antonShkaplerov);
            expedition43.Astronauts.Add(samanthaCristoforetti);
            expedition43.Id = 0;
            Mission spXdm2 = new Mission("SpaceX Demonstration Mission 2", Type.Other);
            spXdm2.Astronauts.Add(dougHurley);
            spXdm2.Astronauts.Add(robertBehnken);
            spXdm2.Id = 1;#1#
            
            Astronauts.Add(dougHurley);
            Astronauts.Add(robertBehnken);
            Astronauts.Add(chrisHadfield);
            Astronauts.Add(samanthaCristoforetti);
            Astronauts.Add(antonShkaplerov);*/

            /*Missions.Add(expedition43);
            Missions.Add(spXdm2);*/
        }
    }
}