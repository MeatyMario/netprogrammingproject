﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Spaceflight.BL.Domain;

namespace Spaceflight.DAL.EF
{
    public class MissionInitializer
    {
        private static bool _initialisationDbHappened;
        
        public static void Initialize(MissionDbContext context, bool dropCreateDatabase = true)
        {
            if (!_initialisationDbHappened)
            {
                if (dropCreateDatabase)
                    context.Database.EnsureDeleted();
                if (context.Database.EnsureCreated())
                {
                    Seed(context);
                    _initialisationDbHappened = true;
                }
                    
            }
        }

        public static void Seed(MissionDbContext context)
        {
            //Make Agencies
            Agency nasa = new Agency()
            {
                Id = 1,
                Name = "NASA",
                Country = "United States of America",
                Budget = 22.6
            };
            Agency csa = new Agency()
            {
                Id = 2,
                Name = "CSA",
                Country = "Canada",
                Budget = 0.332
            };
            Agency esa = new Agency()
            {
                Id = 3,
                Name = "ESA",
                Country = "European Union",
                Budget = 6.68
            };
            Agency roscosmos = new Agency()
            {
                Id = 4,
                Name = "Roscosmos",
                Country = "Russia",
                Budget = 2.4
            };

            //Make Astronauts
            Astronaut dougHurley = new Astronaut()
            {
                Id = 1,
                Name ="Douglas Hurley",
                BirthYear = new DateTime(1966,10,21),
                Gender = "Male",
                Agency = nasa,
                Missions = new List<AstronautMission>()
            };
            Astronaut robertBehnken = new Astronaut()
            {
                Id = 2,
                Name ="Robert Behnken",
                BirthYear = new DateTime(1970,7,28),
                Gender = "Male",
                Agency = nasa,
                Missions = new List<AstronautMission>()
            };
            Astronaut chrisHadfield = new Astronaut()
            {
                Id = 3,
                Name ="Chris Hadfield",
                BirthYear = new DateTime(1959,8,29),
                Gender = "Male",
                Agency = csa,
                Missions = new List<AstronautMission>()
            };
            Astronaut samanthaCristoforetti = new Astronaut()
            {
                Id = 4,
                Name ="Samantha Cristoforetti",
                BirthYear = new DateTime(1977,4,26),
                Gender = "Female",
                Agency = esa,
                Missions = new List<AstronautMission>()
            };
            Astronaut antonShkaplerov = new Astronaut()
            {
                Id = 5,
                Name ="Anton Shkaplerov",
                BirthYear = new DateTime(1972,2,20),
                Gender = "Male",
                Agency = roscosmos,
                Missions = new List<AstronautMission>()
            };
            Astronaut terryWVirts = new Astronaut()
            {
                Id = 6,
                Name ="Terry W. Virts",
                BirthYear = new DateTime(1967,12,1),
                Gender = "Male",
                Agency = nasa,
                Missions = new List<AstronautMission>()
            };
            Astronaut neilArmstrong = new Astronaut()
            {
                Id = 7,
                Name ="Neil Armstrong",
                BirthYear = new DateTime(1930,8,5),
                Gender = "Male",
                Agency = nasa,
                Missions = new List<AstronautMission>()
            };
            Astronaut buzzAldrin = new Astronaut()
            {
                Id = 8,
                Name ="Buzz Aldrin",
                BirthYear = new DateTime(1930,1,20),
                Gender = "Male",
                Agency = nasa,
                Missions = new List<AstronautMission>()
            };
            Astronaut michaelCollins = new Astronaut()
            {
                Id = 9,
                Name ="Michael Collins",
                BirthYear = new DateTime(1930,10,31),
                Gender = "Male",
                Agency = nasa,
                Missions = new List<AstronautMission>()
            };
            Astronaut robertThirsk = new Astronaut()
            {
                Id = 10,
                Name ="Robert Thirsk",
                BirthYear = new DateTime(1953,8,17),
                Gender = "Male",
                Agency = csa,
                Missions = new List<AstronautMission>()
            };
            Astronaut davidSaintJacques = new Astronaut()
            {
                Id = 11,
                Name ="David Saint-Jacques",
                BirthYear = new DateTime(1970,1,6),
                Gender = "Male",
                Agency = csa,
                Missions = new List<AstronautMission>()
            };
            Astronaut anneMcClain = new Astronaut()
            {
                Id = 12,
                Name ="Anne McClain",
                BirthYear = new DateTime(1979,6,7),
                Gender = "Female",
                Agency = nasa,
                Missions = new List<AstronautMission>()
            };
            Astronaut olegKononenko = new Astronaut()
            {
                Id = 13,
                Name ="Oleg Kononenko",
                BirthYear = new DateTime(1964,6,21),
                Gender = "Male",
                Agency = roscosmos,
                Missions = new List<AstronautMission>()
            };
            Astronaut thomasPesquet = new Astronaut()
            {
                Id = 14,
                Name ="Thomas Pesquet",
                BirthYear = new DateTime(1978,2,27),
                Gender = "Male",
                Agency = esa,
                Missions = new List<AstronautMission>()
            };
            Astronaut alexanderGerst = new Astronaut()
            {
                Id = 15,
                Name ="Alexander Gerst",
                BirthYear = new DateTime(1976,5,3),
                Gender = "Male",
                Agency = esa,
                Missions = new List<AstronautMission>()
            };
            Astronaut romanRomanenko = new Astronaut()
            {
                Id = 16,
                Name ="Roman Romanenko",
                BirthYear = new DateTime(1971,8,9),
                Gender = "Male",
                Agency = roscosmos,
                Missions = new List<AstronautMission>()
            };
            Astronaut thomasMashburn = new Astronaut()
            {
                Id = 17,
                Name ="Thomas Mashburn",
                BirthYear = new DateTime(1960,8,29),
                Gender = "Male",
                Agency = nasa,
                Missions = new List<AstronautMission>()
            };
            Astronaut frankDeWinne = new Astronaut()
            {
                Id = 18,
                Name ="Frank de Winne",
                BirthYear = new DateTime(1961,4,25),
                Gender = "Male",
                Agency = esa,
                Missions = new List<AstronautMission>()
            };
            Astronaut maksimSurayev = new Astronaut()
            {
                Id = 19,
                Name ="Maksim Surayev",
                BirthYear = new DateTime(1972,5,24),
                Gender = "Male",
                Agency = roscosmos,
                Missions = new List<AstronautMission>()
            };
            Astronaut gregoryWiseman = new Astronaut()
            {
                Id = 20,
                Name ="Gregory Reid Wiseman",
                BirthYear = new DateTime(1975,11,11),
                Gender = "Male",
                Agency = nasa,
                Missions = new List<AstronautMission>()
            };

            //Make Missions
            Mission expedition43 = new Mission()
            {
                Id = 1,
                MissionName = "Expedition 43",
                MissionType = MissionType.Transport,
                Astronauts = new List<AstronautMission>()
            };
            
            Mission spXdm2 = new Mission()
            {
                Id = 2,
                MissionName = "SpaceX Demonstration Mission 2",
                MissionType = MissionType.Other,
                Astronauts = new List<AstronautMission>()
            };

            Mission apollo11 = new Mission()
            {
                Id = 3,
                MissionName = "Apollo 11",
                MissionType = MissionType.Exploration,
                Astronauts = new List<AstronautMission>()
            };
            
            Mission soyuzTma07M = new Mission()
            {
                Id = 4,
                MissionName = "Soyuz TMA-07M",
                MissionType = MissionType.Transport,
                Astronauts = new List<AstronautMission>()
            };
            
            Mission soyuzTma13M = new Mission()
            {
                Id = 5,
                MissionName = "Soyuz TMA-13M",
                MissionType = MissionType.Transport,
                Astronauts = new List<AstronautMission>()
            };
            
            Mission soyuzTma15 = new Mission()
            {
                Id = 6,
                MissionName = "Soyuz TMA-15",
                MissionType = MissionType.Transport,
                Astronauts = new List<AstronautMission>()
            };
            
            Mission soyuzMs11 = new Mission()
            {
                Id = 7,
                MissionName = "Soyuz MS-11",
                MissionType = MissionType.Transport,
                Astronauts = new List<AstronautMission>()
            };
            
            Mission voyager1 = new Mission()
            {
                Id = 8,
                MissionName = "Voyager 1",
                MissionType = MissionType.Scientific,
                Astronauts = new List<AstronautMission>()
            };
            
            Mission voyager2 = new Mission()
            {
                Id = 9,
                MissionName = "Voyager 2",
                MissionType = MissionType.Scientific,
                Astronauts = new List<AstronautMission>()
            };
            
            Mission perseverance = new Mission()
            {
                Id = 10,
                MissionName = "Perseverance Mission",
                MissionType = MissionType.Scientific,
                Astronauts = new List<AstronautMission>()
            };
            
            Mission pioneer10 = new Mission()
            {
                Id = 11,
                MissionName = "Pioneer 10",
                MissionType = MissionType.Exploration,
                Astronauts = new List<AstronautMission>()
            };
            
            Mission pathfinder = new Mission()
            {
                Id = 12,
                MissionName = "Mars Pathfinder",
                MissionType = MissionType.Exploration,
                Astronauts = new List<AstronautMission>()
            };
            
            Mission crs1 = new Mission()
            {
                Id = 13,
                MissionName = "CRS-1",
                MissionType = MissionType.Resupply,
                Astronauts = new List<AstronautMission>()
            };
            
            Mission crs12 = new Mission()
            {
                Id = 14,
                MissionName = "CRS-12",
                MissionType = MissionType.Resupply,
                Astronauts = new List<AstronautMission>()
            };
            
            Mission crs20 = new Mission()
            {
                Id = 15,
                MissionName = "CRS-20",
                MissionType = MissionType.Resupply,
                Astronauts = new List<AstronautMission>()
            };

            context.AstronautMission.Add(new AstronautMission
            {
                Astronaut = antonShkaplerov,
                Mission = expedition43
            });
            
            context.AstronautMission.Add(new AstronautMission
            {
                Astronaut = samanthaCristoforetti,
                Mission = expedition43
            });
            
            context.AstronautMission.Add(new AstronautMission
            {
                Astronaut = terryWVirts,
                Mission = expedition43
            });
            
            context.AstronautMission.Add(new AstronautMission
            {
                Astronaut = dougHurley,
                Mission = spXdm2
            });
            
            context.AstronautMission.Add(new AstronautMission
            {
                Astronaut = robertBehnken,
                Mission = spXdm2
            });
            
            context.AstronautMission.Add(new AstronautMission
            {
                Astronaut = neilArmstrong,
                Mission = apollo11
            });
            
            context.AstronautMission.Add(new AstronautMission
            {
                Astronaut = buzzAldrin,
                Mission = apollo11
            });
            
            context.AstronautMission.Add(new AstronautMission
            {
                Astronaut = michaelCollins,
                Mission = apollo11
            });
            
            context.AstronautMission.Add(new AstronautMission
            {
                Astronaut = chrisHadfield,
                Mission = soyuzTma07M
            });
            
            context.AstronautMission.Add(new AstronautMission
            {
                Astronaut = thomasMashburn,
                Mission = soyuzTma07M
            });
            
            context.AstronautMission.Add(new AstronautMission
            {
                Astronaut = romanRomanenko,
                Mission = soyuzTma07M
            });
            
            context.AstronautMission.Add(new AstronautMission
            {
                Astronaut = maksimSurayev,
                Mission = soyuzTma13M
            });
            
            context.AstronautMission.Add(new AstronautMission
            {
                Astronaut = gregoryWiseman,
                Mission = soyuzTma13M
            });
            
            context.AstronautMission.Add(new AstronautMission
            {
                Astronaut = alexanderGerst,
                Mission = soyuzTma13M
            });
            
            context.AstronautMission.Add(new AstronautMission
            {
                Astronaut = romanRomanenko,
                Mission = soyuzTma15
            });
            
            context.AstronautMission.Add(new AstronautMission
            {
                Astronaut = frankDeWinne,
                Mission = soyuzTma15
            });
            
            context.AstronautMission.Add(new AstronautMission
            {
                Astronaut = robertThirsk,
                Mission = soyuzTma15
            });
            
            context.AstronautMission.Add(new AstronautMission
            {
                Astronaut = olegKononenko,
                Mission = soyuzMs11
            });
            
            context.AstronautMission.Add(new AstronautMission
            {
                Astronaut = davidSaintJacques,
                Mission = soyuzMs11
            });
            
            context.AstronautMission.Add(new AstronautMission
            {
                Astronaut = anneMcClain,
                Mission = soyuzMs11
            });
            
            
            

            context.Agencies.Add(nasa);
            context.Agencies.Add(csa);
            context.Agencies.Add(esa);
            context.Agencies.Add(roscosmos);

            context.Astronauts.Add(dougHurley);
            context.Astronauts.Add(robertBehnken);
            context.Astronauts.Add(chrisHadfield);
            context.Astronauts.Add(samanthaCristoforetti);
            context.Astronauts.Add(antonShkaplerov);
            context.Astronauts.Add(terryWVirts);
            context.Astronauts.Add(neilArmstrong);
            context.Astronauts.Add(buzzAldrin);
            context.Astronauts.Add(michaelCollins);
            context.Astronauts.Add(robertThirsk);
            context.Astronauts.Add(davidSaintJacques);
            context.Astronauts.Add(anneMcClain);
            context.Astronauts.Add(olegKononenko);
            context.Astronauts.Add(thomasPesquet);
            context.Astronauts.Add(alexanderGerst);
            context.Astronauts.Add(romanRomanenko);
            context.Astronauts.Add(thomasMashburn);
            context.Astronauts.Add(frankDeWinne);
            
            context.Missions.Add(expedition43);
            context.Missions.Add(spXdm2);
            context.Missions.Add(apollo11);
            context.Missions.Add(soyuzTma07M);
            context.Missions.Add(soyuzTma13M);
            context.Missions.Add(soyuzTma15);
            context.Missions.Add(soyuzMs11);
            context.Missions.Add(voyager1);
            context.Missions.Add(voyager2);
            context.Missions.Add(perseverance);
            context.Missions.Add(pioneer10);
            context.Missions.Add(pathfinder);
            context.Missions.Add(crs1);
            context.Missions.Add(crs12);
            context.Missions.Add(crs20);

            context.SaveChanges();

            foreach (var entry in context.ChangeTracker.Entries().ToList())
            {
                entry.State = EntityState.Detached;
            }
        }
    }
}