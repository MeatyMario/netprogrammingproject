﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Spaceflight.BL.Domain;

namespace Spaceflight.DAL.EF
{
    public class Repository : IRepository
    {
        private MissionDbContext _dbContext;

        public Repository()
        {
            _dbContext = new MissionDbContext();
        }

        public Astronaut ReadAstronaut(int id)
        {
            return _dbContext.Astronauts.Find(id);
        }

        public Mission ReadMission(int id)
        {
            return _dbContext.Missions.Find(id);
        }

        public Agency ReadAgency(int id)
        {
            return _dbContext.Agencies.Find(id);
        }

        public IEnumerable<Astronaut> ReadAllAstronauts()
        {
            return _dbContext.Astronauts;
        }

        public IEnumerable<Mission> ReadAllMissions()
        {
            return _dbContext.Missions;
        }

        public IEnumerable<Astronaut> ReadAstronautsByNameAndDob(string name = null, string dob = null)
        {
            IQueryable<Astronaut> astronautQueryable = _dbContext.Astronauts;

            if (name != null)
            {
                astronautQueryable = astronautQueryable.Where(a => a.Name.ToLower().Contains(name.ToLower()));
            }

            if (dob != null)
            {
                astronautQueryable = astronautQueryable.Where(a => a.BirthYear == Convert.ToDateTime(dob));
            }


            return astronautQueryable;

        }

        public IEnumerable<Mission> ReadMissionsByType(MissionType missionType)
        {
            return _dbContext.Missions.Where(m => m.MissionType == missionType);
        }

        public void CreateAstronaut(Astronaut astronaut)
        {
            _dbContext.Astronauts.Add(astronaut);
            _dbContext.SaveChanges();
        }

        public void CreateMission(Mission mission)
        {
            _dbContext.Missions.Add(mission);
           _dbContext.SaveChanges();
        }

        public IEnumerable<Astronaut> ReadAllAstronautsWithAgency()
        {
            return _dbContext.Astronauts
                .Include(astronaut => astronaut.Agency)
                .Include(astronaut => astronaut.Missions)
                .ThenInclude(astronautMission => astronautMission.Mission);

        }

        public IEnumerable<Mission> ReadAllMissionsWithAstronauts()
        {
            return _dbContext.Missions
                .Include(mission => mission.Astronauts)
                .ThenInclude(astronautMission => astronautMission.Astronaut);
        }

        public void CreateAstronautMission(AstronautMission astronautMission)
        {
            _dbContext.AstronautMission.Add(astronautMission);
            _dbContext.SaveChanges();
        }

        public void DeleteAstronautMission(int astronautId, int missionId)
        {
            var astronautMission = _dbContext.AstronautMission
                .Single(am => am.Astronaut.Id == astronautId
                               && am.Mission.Id == missionId);
            _dbContext.AstronautMission.Remove(astronautMission);
            _dbContext.SaveChanges();
        }

        public IEnumerable<Mission> ReadMissionsWithAstronaut(int id)
        {
            return _dbContext.AstronautMission
                .Where(astronaut => astronaut.Astronaut.Id == id)
                .Select(a => a.Mission);
        }

        public IEnumerable<Mission> ReadAstronautWithMissions(int id)
        {
            return _dbContext.AstronautMission
                .Where(a => a.Astronaut.Id == id)
                .Include(a => a.Astronaut)
                .Select(m => m.Mission);
        }

        public IEnumerable<Mission> ReadAstronautWithoutMissions(int id)
        {
            ICollection<Mission> missions =  new Collection<Mission>();
            HashSet<int> notNeededId = new HashSet<int>();

            List<AstronautMission> am = _dbContext.AstronautMission
                .Include(a => a.Astronaut)
                .Include(a => a.Mission)
                .ToList();

            foreach (var astronautMission in am)
            {
                if (astronautMission.Astronaut.Id == id)
                {
                    notNeededId.Add(astronautMission.Mission.Id);
                }
            }
            foreach (var a in _dbContext.Missions)
            {
                if (!notNeededId.Contains(a.Id))
                    missions.Add(a);
            }

            return missions.AsEnumerable();
        }

        public IEnumerable<Agency> ReadAllAgencies()
        {
            return _dbContext.Agencies;
        }
    }
}