﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Spaceflight.BL.Domain;

namespace Spaceflight.DAL.EF
{
    public class MissionDbContext : DbContext
    {
        public MissionDbContext()
        {
            MissionInitializer.Initialize(this, true);
        }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlite("Data Source=../mission.db")
                    .UseLoggerFactory(LoggerFactory.Create(builder => builder.AddDebug()));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Relatie tussen Astronaut & Agency: 1-op-N
            modelBuilder.Entity<BL.Domain.Astronaut>()
                .HasOne(astronaut => astronaut.Agency)
                .WithMany(agency => agency.Astronauts)
                .IsRequired();

            
            //Relatie tussen Astronaut & Mission: N-op-N
            modelBuilder.Entity<Mission>().HasMany(a => a.Astronauts);
            modelBuilder.Entity<AstronautMission>()
                .HasOne(a => a.Mission)
                .WithMany(m => m.Astronauts)
                .HasForeignKey("MissionFK_shadow")
                .IsRequired();
            modelBuilder.Entity<AstronautMission>()
                .HasOne(a => a.Astronaut)
                .WithMany(m => m.Missions)
                .HasForeignKey("AstronautFK_shadow")
                .IsRequired();
            modelBuilder.Entity<AstronautMission>()
                .HasKey("MissionFK_shadow", "AstronautFK_shadow");
        }

        public DbSet<Astronaut> Astronauts { get; set; }
        public DbSet<Agency> Agencies { get; set; }
        public DbSet<Mission> Missions { get; set; }
        public DbSet<AstronautMission> AstronautMission { get; set; }
    }
}