﻿using System;
using System.ComponentModel.DataAnnotations;
using Spaceflight.BL.Domain;

namespace Spaceflight.UI.MVC.Models
{
    public class NewAstronautViewModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public DateTime BirthYear { get; set; }
        [Required]
        public string Gender { get; set; }
        [Required]
        public int Id { get; set; }
    }
}