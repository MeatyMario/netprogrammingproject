﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Spaceflight.BL;
using Spaceflight.BL.Domain;
using Spaceflight.UI.MVC.Models;

namespace Spaceflight.UI.MVC.Controllers
{
    public class AstronautController : Controller
    {
        private IManager _manager;

        public AstronautController()
        {
            _manager = new Manager();
        }
        
        public IActionResult Index()
        {
            return View(_manager.GetAllAstronautsWithAgency());
        }
        
        [HttpGet]
        public IActionResult Add()
        {
            ViewBag.Agency = _manager.GetAllAgencies();
            return View();
        }
        
        [HttpPost]
        public IActionResult Add([FromForm] NewAstronautViewModel astronautViewModel)
        {
            ViewBag.Agency = _manager.GetAllAgencies();
            if (!ModelState.IsValid)
            {
                return View();
            }
            Astronaut a;
            Agency agency = _manager.GetAgency(astronautViewModel.Id);
            a = _manager.AddAstronaut(astronautViewModel.Name, astronautViewModel.BirthYear, astronautViewModel.Gender, agency);
            return RedirectToAction("Detail", new {id = a.Id});
        }
        
        public IActionResult Detail(int id)
        {
            ViewBag.Astronaut = _manager.GetAstronaut(id);
            ViewBag.Mission = _manager.GetAstronautWithMissions(id);
            ViewBag.UnassignedMission = _manager.GetAstronautWithoutMissions(id);
            return View();
        }

        public IActionResult LinkMission(int astronautId, int missionId)
        { 
            Astronaut chosenAstronaut = _manager.GetAstronaut(astronautId);
            Mission chosenMission = _manager.GetMission(missionId);
            AstronautMission astronautMission = new AstronautMission()
            {
                Astronaut = chosenAstronaut,
                Mission = chosenMission
            };
            _manager.AddAstronautMission(astronautMission);
            return RedirectToAction("Detail", new {id = astronautId});
        }
        
        public IActionResult DeleteMission(int astronautId, int missionId)
        { 
            _manager.RemoveAstronautMission(astronautId, missionId);
            return RedirectToAction("Detail", new {id = astronautId});
        }

        [HttpGet]
        public IActionResult FilterOnNameAndDOB(string name, string dob)
        {
            IEnumerable<Astronaut> astronauts = _manager.GetAstronautsByNameAndDob(name, dob);
            return View(astronauts);
        }
        
    }
}