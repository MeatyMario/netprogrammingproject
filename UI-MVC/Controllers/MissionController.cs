﻿using Microsoft.AspNetCore.Mvc;
using Spaceflight.BL;

namespace Spaceflight.UI.MVC.Controllers
{
    public class MissionController : Controller
    {
        private IManager _manager;

        public MissionController()
        {
            _manager = new Manager();
        }

        
        public IActionResult Details(int id)
        {
            return View(_manager.GetMission(id));
        }
    }
}